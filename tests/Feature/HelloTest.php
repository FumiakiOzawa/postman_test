<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HelloTest extends TestCase
{
    public function test_api_helloにアクセスして200が返却されること()
    {
        $response = $this->get('/api/hello');

        $response->assertStatus(200);
    }

    public function test_api_helloにアクセスしてhello_worldが返却されること()
    {
        $response = $this->json('GET', '/api/hello');

        $response->assertJson(['message' => 'Hello world']);
    }

    public function test_api_helloOzawaにアクセスしてhello_ozawaが返却されること()
    {
        $response = $this->json('GET', '/api/hello/ozawa');

        $response->assertJson(['message' => 'Hello ozawa']);
    }

    public function test_api_hello_yazawaにアクセスしてhello_yazawaが返却されること()
    {
        $response = $this->json('GET', '/api/hello/yazawa');

        $response->assertJson(['message' => 'Hello yazawa']);
    }
}
